class mnProc {

    public:

    // members -- note, these need to be defined in the same order in which they appear in the branch declaration
    double t[18]; double x[18]; double y[18]; double z[18]; double q[18]; // t, x, y, z, q for individual neutron hit events   
    int hitvec[16]; // hit vector (index is hit number)
    int clusttype[16]; // cluster type vector (index is cluster type (1=singlet, 2=doublet, 3=triplet, etc.)) histogrammed for each event
    int ordervec[16]; // z orientation vector (index is hit number)
    double nsi12; double nsi13; double nsi23; double a012; double a013; double a023; double d12; double d13; double d23; double t12; double t13; double t23; double v12; double v13; double v23;// ansi plot variables for first 2 or 3 neutrons
    int ordertype[4]; // z orientation vector (index is scatter type (0=singlet, 1=back scatter, 2=vertical scatter, 3=forward scatter)) histogrammed for each event
    int nmult; // final neutron multiplicity 
    int multn1; int multn2; int multn3; // simulation only: original multiplicity of the first, second, and third neutrons
    bool quad; bool dd; bool ds; bool pt; bool pa; bool cc; // flags describing causal test parameters
    int ordercheck;

//    double td[16][16]; // time difference
//    double d[16][16]; // pathlength
//    double v[16][16]; // velocity between hits
//    double a0[16][16]; // angle between hits
//    double nsi[16][16]; // neutron spacetime interval
    float vbeam; // neutron beam velocity

    // event structure
    typedef struct {
      double t[18]; double x[18]; double y[18]; double z[18]; double q[18]; 
//      double a0[16][16]; double nsi[16][16];
      int ordercheck; int mult;
    } MNEVT;

    // methods
    int quadrangleAnsiCheck(MNEVT, double, double, double, double, double, double); // angle-nsi check for causal relation between hits
    int rikenCheck(MNEVT, int, int, float, float, float, float, float); // causality cut mimicking how RIKEN checks causal relations
    int causCheck(MNEVT, float, float, float); // former causality cut check
    int qRescueCheck(MNEVT, int, int, float); // checks if q values are high enough to warrant the scatter is actually 2 separate neutrons
    int ptCheck(MNEVT, int, int, float, float, float, float, float, float); // remove 1ns for having shorter attenuation length, as function of bar combination
    int mnReset();
};
