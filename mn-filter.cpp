/**********
mn-filter version 1.2

Description: The following code sorts ROOT files (MoNA data or simulation data) and adds new branches with filtered neutron multiplicity and hit data. It applies a q threshold to remove gamma ray events if important, then evaluates proton crosstalk clusters (neighboring events in spacetime) and determines which hit in a cluster was the true first hit. Then it causal relationship between all remaining hits to determine whether hits are causally connected (by angle-nsi algorithm or traditional causality cut). It outputs remaining hits, which must correspond to actual neutrons, to a new branch in the output ROOT file called "mn".  

Authors: Warren Rogers and Andrea Munroe, Indiana Wesleyan University, June 2021
**********/

#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include <math.h>
#include "mn-filter.h"

// TODO: environment variables?

// root header files
#include </home/rogers/root/include/TFile.h>
#include </home/rogers/root/include/TTree.h>
#include </home/rogers/root/include/TTreeIndex.h>
#include </home/rogers/root/include/TBranchElement.h>
#include </home/rogers/root/include/TH1.h>
using namespace std;

// ***** variables for control of the filtering: doublet radius, separation radius, doublet time diff, q cut, nsi and angle cuts
float clustrad;
float seprad;
float ccutrad;
float dtdiff;
float nsicut;
float angmax;
float ebeam;
float g_qcut;
float ds_qcut; // later put this in the config file
double td[16][16]; // time difference
double d[16][16]; // pathlength
double v[16][16]; // velocity between hits
double a0[16][16]; // angle between hits
double nsi[16][16]; // neutron spacetime interval
int multn1;
int multn2;
int multn3;
int ncount; // mn neutrons
//int zcount;// zombies (diagnostic)
int ccount; // mn-causality cut (diagnostic)
int scount; // scattering sites (diagnostic)
int qcount; // quadrangles
int pcount; // pacut
int quad1, quad2, quad3, quad4, ptcut, pacut, ccuttest, dubdub, dubsing; // ANSI quadrangle flags used in filter
int n1, n2, n3;

double t1, t2, t3, t4, t5, t6, t7, t8, t9, t10;
double x1, x2, x3, x4, x5, x6, x7, x8, x9, x10;
double y01, y2, y3, y4, y5, y6, y7, y8, y9, y10;
double z1, z2, z3, z4, z5, z6, z7, z8, z9, z10;
double q1, q2, q3, q4, q5, q6, q7, q8, q9, q10;

//arrays of line segment data for quadrangle ansi cut
double px[16];
double py[16];
double m[16];
double b[16];

// labels for extracting simulation branch data from ROOT
const char* sim_t[10] = {"b13pg1t","b13pg2t","b13pg3t","b13pg4t","b13pg5t","b13pg6t","b13pg7t","b13pg8t","b13pg9t","b13pg10t"};
const char* sim_x[10] = {"b13pg1x","b13pg2x","b13pg3x","b13pg4x","b13pg5x","b13pg6x","b13pg7x","b13pg8x","b13pg9x","b13pg10x"};
const char* sim_y[10] = {"b13pg1y","b13pg2y","b13pg3y","b13pg4y","b13pg5y","b13pg6y","b13pg7y","b13pg8y","b13pg9y","b13pg10y"};
const char* sim_z[10] = {"b13pg1z","b13pg2z","b13pg3z","b13pg4z","b13pg5z","b13pg6z","b13pg7z","b13pg8z","b13pg9z","b13pg10z"};
const char* sim_q[10] = {"b13pg1Edep","b13pg2Edep","b13pg3Edep","b13pg4Edep","b13pg5Edep","b13pg6Edep","b13pg7Edep","b13pg8Edep","b13pg9Edep","b13pg10Edep"};

// array for storing LANL hit data
double lanl_mult;
double lanl_t[17];
double lanl_x[17];
double lanl_y[17];
double lanl_z[17];
double lanl_q[17];
double lanl_t0t;

int main(int argc, char** argv)
{ 
//  if (argc < 5) {
// Tell the user how to run the program
//    std::cerr << "Usage: " << argv[0] << " infile, outfile, data(1) or simulation (2) or LANL data(3), dd/ds inside quadrangles (1) or pa-cut inside quadrangles (2) or full quandrangle cut (3), config file (optional, default = ~/mn-filter.config)" << std::endl;
//    return 0;
//  }  

  mnProc mn; // create multi-neutron object
  mnProc caus; // create multi-neutron object for mn-causality cut (diagnostic)
  mnProc dco; // create object for de-clustered events but with no causality cut
  mnProc quad; // create object for eliminating events within up to 4 quadrangles in ANSI landscape
  mnProc pa; // create object for multi-neutron events based on position-angle landscape cut
  mnProc::MNEVT nevt; // create neutron-event structure

// ##########
//
// read in configuration variables
//
// ##########

  ifstream config_file;
  string config_file_name[2];
  if(argc == 5){ //check for optional config file
    config_file_name[1] = argv[4];
  } else {
    config_file_name[1] = "~/Dropbox/mn-filter-src/mn-filter.config"; //default config file name
    config_file_name[0] = "~/";
  }
  config_file.open(config_file_name[1],ios::in);
  string inputline;
  string inputline1;
  string inputline2;
  string inputline3;
  string inputline4;
  if (config_file.is_open()) {
    getline(config_file,inputline);
    istringstream in(inputline);
    in >> clustrad >> seprad >> ccutrad >> dtdiff >> nsicut >> angmax >> ebeam >> g_qcut >> ds_qcut;

    getline(config_file,inputline1); 
    istringstream in1(inputline1);
    in1 >> px[0]>>py[0]>>px[1]>>py[1]>>px[2]>>py[2]>>px[3]>>py[3]; 
    
    getline(config_file,inputline2); 
    istringstream in2(inputline2);
    in2 >> px[4]>>py[4]>>px[5]>>py[5]>>px[6]>>py[6]>>px[7]>>py[7];
    
    getline(config_file,inputline3); 
    istringstream in3(inputline3);
    in3 >> px[8]>>py[8]>>px[9]>>py[9]>>px[10]>>py[10]>>px[11]>>py[11];
    
    getline(config_file,inputline4); 
    istringstream in4(inputline4);
    in4 >> px[12]>>py[12]>>px[13]>>py[13]>>px[14]>>py[14]>>px[15]>>py[15];
    
    mn.vbeam = 29.98*sqrt(1-pow(939.57/(ebeam+939.57),2)); // conversion from neutron ebeam to vbeam
    config_file.close();
  } else {
    cout << "Failed to open config file" << endl;
    return 0;
  }

// ##########
//
// Open root file, clone tree, assign branches
//
// ##########

  char* infile = argv[1];
  TFile *ifile = new TFile(infile,"UPDATE");
  TTree *itree;
  if(strtol(argv[3], NULL, 10) == 3){ //  tree "tree"
    itree = (TTree*)ifile->Get("tree");
  } else if(strtol(argv[3], NULL, 10) == 4){ // tree "tcal"
  } else {
    itree = (TTree*)ifile->Get("t");
  }
  char* outfile = argv[2];  
  int datatype = strtol(argv[3], NULL, 10); // 1 for experimental data, 2 for simulation data
  TFile* ofile = new TFile(outfile,"recreate");
//  int causaltest = strtol(argv[4], NULL, 10); // 1 for angle-nsi causal test, 2 for causality cut test
  if(datatype == 1 || datatype == 3 || datatype == 4){ cout << "NOTE: disregard any warnings above pertaining to fragment dictionaries."<<endl<<endl;} // eventually something to fix?
  
  cout << "\nBegin cloning tree" << endl;
  TTree* mnt = itree->CloneTree();
  cout << "Finished cloning tree" << endl;

  // Create branch for multi-neutron data:
  TBranch *bmn = mnt->Branch("mn",&mn,"t[18]/D:x[18]/D:y[18]/D:z[18]/D:q[18]/D:hitvec[16]/I:clusttype[16]/I:ordervec[16]/I:nsi12/D:nsi13/D:nsi23/D:a012/D:a013/D:a023/D:d12/D:d13/D:d23/D:t12/D:t13/D:t23/D:v12/D:v13/D:v23/D:ordertype[4]/I:nmult/I:multn1/I:multn2/I:multn3/I:quad/O:dd/O:ds/O:pt/O:pa/O:cc/O");
  TBranch *bdco = mnt->Branch("dco",&dco,"t[18]/D:x[18]/D:y[18]/D:z[18]/D:q[18]/D:hitvec[16]/I:clusttype[16]/I:ordervec[16]/I:nsi12/D:nsi13/D:nsi23/D:a012/D:a013/D:a023/D:d12/D:d13/D:d23/D:t12/D:t13/D:t23/D:v12/D:v13/D:v23/D:ordertype[4]/I:nmult/I");
  TBranch *bcaus = mnt->Branch("caus",&caus,"t[18]/D:x[18]/D:y[18]/D:z[18]/D:q[18]/D:hitvec[16]/I:clusttype[16]/I:ordervec[16]/I:nsi12/D:nsi13/D:nsi23/D:a012/D:a013/D:a023/D:d12/D:d13/D:d23/D:t12/D:t13/D:t23/D:v12/D:v13/D:v23/D:ordertype[4]/I:nmult/I");
  TBranch *bquad = mnt->Branch("quad",&quad,"t[18]/D:x[18]/D:y[18]/D:z[18]/D:q[18]/D:hitvec[16]/I:clusttype[16]/I:ordervec[16]/I:nsi12/D:nsi13/D:nsi23/D:a012/D:a013/D:a023/D:d12/D:d13/D:d23/D:t12/D:t13/D:t23/D:v12/D:v13/D:v23/D:ordertype[4]/I:nmult/I");
  TBranch *bpa = mnt->Branch("pa",&pa,"t[18]/D:x[18]/D:y[18]/D:z[18]/D:q[18]/D:hitvec[16]/I:clusttype[16]/I:ordervec[16]/I:nsi12/D:nsi13/D:nsi23/D:a012/D:a013/D:a023/D:d12/D:d13/D:d23/D:t12/D:t13/D:t23/D:v12/D:v13/D:v23/D:ordertype[4]/I:nmult/I");
// rightcurly 

// access experimental data branches:
  TBranchElement *bm = (TBranchElement*)mnt->GetBranch("mult");
  TBranchElement *bt = (TBranchElement*)mnt->GetBranch("hit.t[10]"); 
  TBranchElement *bx = (TBranchElement*)mnt->GetBranch("hit.x[10]");
  TBranchElement *by = (TBranchElement*)mnt->GetBranch("hit.y[10]");
  TBranchElement *bz = (TBranchElement*)mnt->GetBranch("hit.z[10]");
  TBranchElement *bq = (TBranchElement*)mnt->GetBranch("hit.q[10]");

  if (datatype == 2) { // editing branch structure for simulation data
  
    //references for branch variables, used for loop below
    double* sim_t_ref[10] = {&t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, &t10};
    double* sim_x_ref[10] = {&x1, &x2, &x3, &x4, &x5, &x6, &x7, &x8, &x9, &x10};
    double* sim_y_ref[10] = {&y01, &y2, &y3, &y4, &y5, &y6, &y7, &y8, &y9, &y10};
    double* sim_z_ref[10] = {&z1, &z2, &z3, &z4, &z5, &z6, &z7, &z8, &z9, &z10};
    double* sim_q_ref[10] = {&q1, &q2, &q3, &q4, &q5, &q6, &q7, &q8, &q9, &q10};


    // access simulation data branches:
    if(itree->GetBranch("b13pn3gmultiplicity")!=NULL){ //for 3n+ sim files
      mnt->SetBranchAddress("b13pn3gmultiplicity",&multn3);
    }
    if(itree->GetBranch("b13pn2gmultiplicity")!=NULL){ // for 2n+ sim files
      mnt->SetBranchAddress("b13pn1gmultiplicity",&multn1); 
      mnt->SetBranchAddress("b13pn2gmultiplicity",&multn2);
    } else { //for 1n sim files
      mnt->SetBranchAddress("b13pgmultiplicity",&multn1);
    }
    for (int i = 0; i < 10; ++i){
      mnt->SetBranchAddress(sim_t[i], sim_t_ref[i]);
      mnt->SetBranchAddress(sim_x[i], sim_x_ref[i]);
      mnt->SetBranchAddress(sim_y[i], sim_y_ref[i]);
      mnt->SetBranchAddress(sim_z[i], sim_z_ref[i]);
      mnt->SetBranchAddress(sim_q[i], sim_q_ref[i]);
    }
  } else if (datatype == 3){
    mnt->SetBranchAddress("mult",&lanl_mult);
    mnt->SetBranchAddress("t0t",&lanl_t0t);
    mnt->SetBranchAddress("tmean[17]",&lanl_t); 
    mnt->SetBranchAddress("x[17]",&lanl_x); 
    mnt->SetBranchAddress("y[17]",&lanl_y); 
    mnt->SetBranchAddress("z[17]",&lanl_z); 
    mnt->SetBranchAddress("qmean[17]",&lanl_q); 
  }

  cout << "\nDatafile type: "<<datatype<< " (1=MoNA experiment, 2=simulation, 3=LANL experiment)"<<endl;
//  cout << "Causal test type: "<<causaltest<< " (1=dd/ds inside quarangles, 2=pacut inside quadrangles, 3=cut quadrangles)"<<endl;
  cout << "Using config file: " <<config_file_name[0]<<config_file_name[1]<<endl;
  cout<<"Config variables:  clustrad: "<<clustrad<<"  seprad: "<<seprad<<"  ccutrad: "<<ccutrad<<"  dtdiff: "<<dtdiff<<"  ebeam: "<<ebeam<<"  (vbeam: "<<mn.vbeam<<")"<<" g_qcut: " << g_qcut << " ds_qcut: "<<ds_qcut<<endl; 
  cout<<"Quadrangle 1 coordinates: "<<px[0]<<" "<<py[0]<<" "<<px[1]<<" "<<py[1]<<" "<<px[2]<<" "<<py[2]<<" "<<px[3]<<" "<<py[3]<<endl;
  cout<<"Quadrangle 2 coordinates: "<<px[4]<<" "<<py[4]<<" "<<px[5]<<" "<<py[5]<<" "<<px[6]<<" "<<py[6]<<" "<<px[7]<<" "<<py[7]<<endl;
  cout<<"Quadrangle 3 coordinates: "<<px[8]<<" "<<py[8]<<" "<<px[9]<<" "<<py[9]<<" "<<px[10]<<" "<<py[10]<<" "<<px[11]<<" "<<py[11]<<endl;
  cout<<"Quadrangle 4 coordinates: "<<px[12]<<" "<<py[12]<<" "<<px[13]<<" "<<py[13]<<" "<<px[14]<<" "<<py[14]<<" "<<px[15]<<" "<<py[15]<<endl;

  int nentries = mnt->GetEntries(); // number of entries in the input tree
  cout<<"\nTotal entries: "<<nentries<<"\n\n";

  // calculate slope and intercept of lines between ponts 14 (nsimin), 21 (amax), 32 (nsimax), and 43 (amin) for 3 different quadrangles (used in ansi cut)
  for(int j = 0; j < 4; j++){
	  // slope 14, 21, 32, 43 for each point: m=(x2-x1)/(y2-y1) for f(angle) and m=(y2-y1)/(x2-x1) for f(nsi)
	  if(py[j*4]  !=py[j*4+3]){m[j*4]  = (px[j*4]-  px[j*4+3])/(py[j*4]  -py[j*4+3]);}else{m[j*4]=0;}
	  if(py[j*4+1]!=py[j*4]  ){m[j*4+1]= (py[j*4+1]-py[j*4])  /(px[j*4+1]-px[j*4])  ;}else{m[j*4+1]=0;}
	  if(py[j*4+2]!=py[j*4+1]){m[j*4+2]= (px[j*4+2]-px[j*4+1])/(py[j*4+2]-py[j*4+1]);}else{m[j*4+2]=0;}
	  if(py[j*4+3]!=py[j*4+2]){m[j*4+3]= (py[j*4+3]-py[j*4+2])/(px[j*4+3]-px[j*4+2]);}else{m[j*4+3]=0;}
//	  if(py[j*4+4]!=py[j*4+3]){m[j*4+4]= (py[j*4+4]-py[j*4+3])/(px[j*4+4]-px[j*4+3]);}else{m[j*4+4]=0;}
	  // intercept 14, 21, 32, 43 for each point: y-y1 = m(x-x1) so y=mx + (y1-m*x1) for f(nsi) and x=my + (x1-m*y1)
	  b[j*4] = px[j*4]-m[j*4]*py[j*4];
	  b[j*4+1] = py[j*4+1]-m[j*4+1]*px[j*4+1];
	  b[j*4+2] = px[j*4+2]-m[j*4+2]*py[j*4+2];
	  b[j*4+3] = py[j*4+3]-m[j*4+3]*px[j*4+3];
//	  b[j*4+4] = py[j*4+4]-m[j*4+4]*px[j*4+4];
	  //cout << m[j*4] << " " << b[j*4] << " " << m[j*4+1] << " " << b[j*4+1] << " " << m[j*4+2] << " " << b[j*4+2] << " " << m[j*4+3] << " " << b[j*4+3] << endl;
  }

  // Read cloned tree data and populate the multi-neutron parameters
  Int_t i=0;

  cout << "Begin processing tree data and populating multi-neutron branches" << endl;
  while(i<nentries) {
//  while(i<10000) { 
	mn.mnReset();
	caus.mnReset();
	dco.mnReset();
	quad.mnReset();
	pa.mnReset();
	mnt->GetEntry(i); // get the ith entry in the tree

// ##########
//
// Read and initially process data from experiment or sim file
//
// ##########
//
	// populate the nevt object of the MNEVT structure
	if (datatype == 1) { // experimental data
	  
	  nevt.mult = bm->GetValue(0,0);
	  
	  // assign the event nevt members with their values
	  for (int j=0; j<10; j++) {
	    nevt.t[j] = bt->GetValue(j,0); 
	    nevt.x[j] = bx->GetValue(j,0); 
	    nevt.y[j] = by->GetValue(j,0); 
	    nevt.z[j] = bz->GetValue(j,0); 
	    nevt.q[j] = bq->GetValue(j,0);
	  }

	} else if (datatype == 2) { // simulation data

	  nevt.mult = multn1 + multn2 + multn3; // for 1n, 2n, and 3n sim files (for 1n files, multn2=0, for mult<2 files, multn3=0)
	  mn.multn1 = multn1;
	  mn.multn2 = multn2;
	  mn.multn3 = multn3;
		  
	  // assign the event structure variables with their Entry values
	  nevt.t[0] = t1; nevt.t[1] = t2; nevt.t[2] = t3; nevt.t[3] = t4; nevt.t[4] = t5; // ns
	  nevt.t[5] = t6; nevt.t[6] = t7; nevt.t[7] = t8; nevt.t[8] = t9; nevt.t[9] = t10; // ns
	  nevt.x[0] = 100*x1; nevt.x[1] = 100*x2; nevt.x[2] = 100*x3; nevt.x[3] = 100*x4; nevt.x[4] = 100*x5; // cm
	  nevt.x[5] = 100*x6; nevt.x[6] = 100*x7; nevt.x[7] = 100*x8; nevt.x[8] = 100*x9; nevt.x[9] = 100*x10; // cm
	  nevt.y[0] = 100*y01; nevt.y[1] = 100*y2; nevt.y[2] = 100*y3; nevt.y[3] = 100*y4; nevt.y[4] = 100*y5; // cm
	  nevt.y[5] = 100*y6; nevt.y[6] = 100*y7; nevt.y[7] = 100*y8; nevt.y[8] = 100*y9; nevt.y[9] = 100*y10; // cm
	  nevt.z[0] = 100*z1; nevt.z[1] = 100*z2; nevt.z[2] = 100*z3; nevt.z[3] = 100*z4; nevt.z[4] = 100*z5; // cm
	  nevt.z[5] = 100*z6; nevt.z[6] = 100*z7; nevt.z[7] = 100*z8; nevt.z[8] = 100*z9; nevt.z[9] = 100*z10; // cm
	  nevt.q[0] = q1; nevt.q[1] = q2; nevt.q[2] = q3; nevt.q[3] = q4; nevt.q[4] = q5; 
	  nevt.q[5] = q6; nevt.q[6] = q7; nevt.q[7] = q8; nevt.q[8] = q9; nevt.q[9] = q10;

	} else if (datatype == 3) { // lanl data
		
	//	  nevt.mult = bm->GetValue(0,0);
	  nevt.mult = lanl_mult;
	  for (int j = 0; j < 10; j++){
	    nevt.t[j] = lanl_t[j] - lanl_t0t; // t in LANL data is absolute
	    nevt.x[j] = lanl_x[j];
	    nevt.y[j] = lanl_y[j];
	    nevt.z[j] = lanl_z[j];
	    nevt.q[j] = lanl_q[j];
	  }

	}
	// nevt should now be populated with the event data

	// ***** check hit multiplicity == 1
	if (nevt.mult==1) { // check if single-hit event

	// TODO: hmm, this looks like something you could throw into a function...
	  mn.nmult = 1;
	  mn.hitvec[0]=1;
	  mn.clusttype[1]=1;
	  mn.t[0]=nevt.t[0]; 
	  mn.x[0]=nevt.x[0]; 
	  mn.y[0]=nevt.y[0]; 
	  mn.z[0]=nevt.z[0];
	  mn.q[0]=nevt.q[0];

	  dco.nmult = 1;
	  dco.hitvec[0]=1;
	  dco.clusttype[1]=1;
	  dco.t[0]=nevt.t[0]; 
	  dco.x[0]=nevt.x[0]; 
	  dco.y[0]=nevt.y[0]; 
	  dco.z[0]=nevt.z[0];
	  dco.q[0]=nevt.q[0];
	  
	  caus.nmult = 1;
	  caus.hitvec[0]=1;
	  caus.clusttype[1]=1;
	  caus.t[0]=nevt.t[0]; 
	  caus.x[0]=nevt.x[0]; 
	  caus.y[0]=nevt.y[0]; 
	  caus.z[0]=nevt.z[0];
	  caus.q[0]=nevt.q[0];

	  quad.nmult = 1;
	  quad.hitvec[0]=1;
	  quad.clusttype[1]=1;
	  quad.t[0]=nevt.t[0]; 
	  quad.x[0]=nevt.x[0]; 
	  quad.y[0]=nevt.y[0]; 
	  quad.z[0]=nevt.z[0];
	  quad.q[0]=nevt.q[0];
	  
	  pa.nmult = 1;
	  pa.hitvec[0]=1;
	  pa.clusttype[1]=1;
	  pa.t[0]=nevt.t[0]; 
	  pa.x[0]=nevt.x[0]; 
	  pa.y[0]=nevt.y[0]; 
	  pa.z[0]=nevt.z[0];
	  pa.q[0]=nevt.q[0];
	  
	// ***** check hit multilicity > 1

	} else if (nevt.mult>1 && nevt.mult<11) { // check higher hit multiplicity events 

// ##########
//
// Assign pathlengths, time differences, and effective velocities between all hits.  
// NOTE indices for these three parameters are 1-based (except 0 for target), unlike hits which are 0-based.  
// Populate initial hitvectors
//
// ##########

	  // Calculate distance, time difference, and effective velocity between all hit pairs
	  for (int j=0; j<nevt.mult; j++) {
	    d[0][j+1] = sqrt(nevt.x[j]*nevt.x[j] + nevt.y[j]*nevt.y[j] + nevt.z[j]*nevt.z[j]); // paths from target to all other hits (except last)
	    for (int k=j+1; k<nevt.mult; k++) {
	      td[j+1][k+1] = nevt.t[k] - nevt.t[j]; // time differences between all hit pairs (ns)
	      d[j+1][k+1] = sqrt(pow(nevt.x[k]-nevt.x[j],2)+pow(nevt.y[k]-nevt.y[j],2)+pow(nevt.z[k]-nevt.z[j],2)); // distances between all hit pairs (cm)
	      v[j+1][k+1] = d[j+1][k+1]/td[j+1][k+1]; // propagation velocity between all hit pairs (cm/ns)
	    }
	  }

	  // Fill hit vector to size nevt.mult, set elements to 1
	  for (int j=0; j<nevt.mult; j++) {
	    mn.hitvec[j]=1; // 
	    dco.hitvec[j]=1;
	    caus.hitvec[j]=1; 
	    quad.hitvec[j]=1;
	    pa.hitvec[j]=1;
	  }
//		    cout << mn.hitvec[0] << mn.hitvec[1] << mn.hitvec[2] << mn.hitvec[3] << mn.hitvec[4] << mn.hitvec[5] << endl;
//		    cout << caus.hitvec[0] << caus.hitvec[1] << caus.hitvec[2] << caus.hitvec[3] << caus.hitvec[4] << caus.hitvec[5] << endl;
//		    cout << "***" << endl;

//##########
//
// Check for hit clusters
//
//##########

	  for (int j=0; j<nevt.mult-1; j++) { // check each hit with the two following hits to identify singlet, doublet, triplet events
	    if (mn.hitvec[j]>0) {
  	      for (int k=j+1; k<nevt.mult; k++) { // check the following hits
	        if (mn.hitvec[k]>0) {
//	        for (int k=j+1; k<j+3; k++) { // the two following hits -- &&& later on we can include quadruplet events, but at current beam energies and bar size we see very few
	          if (d[j+1][k+1]<clustrad && td[j+1][k+1]<dtdiff) { // if the hits are within the hypersphere defining a cluster
		    mn.hitvec[j]++; // increment cluster type
		    mn.hitvec[k]=0; // set later cluster hit to 0
		    mn.ordervec[j]=1;
		    mn.ordervec[k]=0;
//	            nevt.ordercheck = round((nevt.z[k]-nevt.z[j])/10) + 2; // checks z values of cluster: 1 = time reversed scatter, 2 = vertical scatter, 3 = forward scatter
//		    if (nevt.ordercheck==1) { // if this event is time reversed
//		      mn.ordervec[k]=1; // mark true first hit as time reversed cluster
//		      mn.ordervec[j]=0; // zero out timing for false first hit
//		      mn.hitvec[k]=mn.hitvec[j]; // put hitvec value in first event
//		      mn.hitvec[j]=0; // clear out hitvec for later event
//		      j=k; // skip all null values until (k,k+1) in loop *** WFR make sure I understand this
//		    } // else if (mn.ordervec[j]==0) { // if this is the first pair of hits in a cluster (i.e. the default value of mn.ordervec = 0 needs updating)

//		    mn.ordervec[j]=nevt.ordercheck; // save ordering value (2 or 3)
		    caus.hitvec[j]=mn.hitvec[j];
		    caus.hitvec[k]=mn.hitvec[k];
		    dco.hitvec[j]=mn.hitvec[j];
		    dco.hitvec[k]=mn.hitvec[k];
		    quad.hitvec[j]=mn.hitvec[j];
		    quad.hitvec[k]=mn.hitvec[k];
		    pa.hitvec[j]=mn.hitvec[j];
		    pa.hitvec[k]=mn.hitvec[k];
	          } // cluster check
	        } // mn.hitvec[k]>0 
	      } // k loop
	    } // mn.hitvec[j]>0
	  } // j loop
	  // So up until now we have extracted data from the original tree, populated the nevt vectors with hit data, and created the modified mn etc. hit vectors with clusters identified


//##########
//
// Test for causal connections and modify hitvec
//
//##########

	  for (int j=0; j<nevt.mult-1; j++) {
	    for (int k=j+1; k<nevt.mult; k++) {
	      quad1 = -1; quad2 = -1; quad3 = -1; quad4 = -1; dubdub = -1; dubsing = -1; ptcut = -1; pacut = -1; ccuttest = -1;
	      // Calculate angle and nsi values for this hit pair and set causal flags for the event
	      a0[j+1][k+1] = 57.29577951*acos(((nevt.x[j]*(nevt.x[k]-nevt.x[j]))+(nevt.y[j]*(nevt.y[k]-nevt.y[j]))+(nevt.z[j]*(nevt.z[k]-nevt.z[j])))/(d[0][j+1]*d[j+1][k+1])); // scattering angle (a0jk)
	      nsi[j+1][k+1] = (mn.vbeam*mn.vbeam)*(td[j+1][k+1]*td[j+1][k+1])-(d[j+1][k+1]*d[j+1][k+1]); // neutron spacetime interval (nsi)
	      //cout << "a012: " << a0[j+1][k+1] << " nsi12: " << nsi[j+1][k+1] << endl;
	      quad1 = mn.quadrangleAnsiCheck(nevt, nsi[j+1][k+1], a0[j+1][k+1], m[0]*a0[j+1][k+1] + b[0], m[1]*nsi[j+1][k+1]+b[1], m[2]*a0[j+1][k+1]+b[2], m[3]*nsi[j+1][k+1]+b[3]);
	      quad2 = mn.quadrangleAnsiCheck(nevt, nsi[j+1][k+1], a0[j+1][k+1], m[4]*a0[j+1][k+1]+ b[4], m[5]*nsi[j+1][k+1]+b[5], m[6]*a0[j+1][k+1]+b[6], m[7]*nsi[j+1][k+1]+b[7]);
	      quad3 = mn.quadrangleAnsiCheck(nevt, nsi[j+1][k+1], a0[j+1][k+1], m[8]*a0[j+1][k+1]+ b[8], m[9]*nsi[j+1][k+1]+b[9], m[10]*a0[j+1][k+1]+b[10], m[11]*nsi[j+1][k+1]+b[11]);
	      quad4 = mn.quadrangleAnsiCheck(nevt, nsi[j+1][k+1], a0[j+1][k+1], m[12]*a0[j+1][k+1]+ b[12], m[13]*nsi[j+1][k+1]+b[13], m[14]*a0[j+1][k+1]+b[14], m[15]*nsi[j+1][k+1]+b[15]);
	      //cout << "quadrangles: " << quad1 << " " << quad2 << " " << quad3 << " " << quad4 << endl;

	      if (mn.hitvec[j]>0 && mn.hitvec[k]>0) { // 
		if (mn.hitvec[j]>1 && mn.hitvec[k]>1) { 
		  dubdub = 1; } else { dubdub = 0; }// 1 if double-double event -- WFR: perhaps later put a dd_qcut on these as well
		if ((mn.hitvec[j]>1 && mn.hitvec[k]==1 && nevt.q[k]>ds_qcut) || (mn.hitvec[k]>1 && mn.hitvec[j]==1 && nevt.q[j]>ds_qcut)) { 
		  dubsing = 1; } else { dubsing = 0; } // 1 if double-single event
	      }
//		This filter uses a (dd || ds) filter for NSI >0 and <10000, and quad cut in quadrangles 
//
//		NOTE: This mn-filter is custom-designed (and hard-wired) for Thomas R. 27O data 9/15/2022
//		This first test eliminates all counts within any of the four quadrangles, each placed where 1n density is high in the simulation data
		if(quad1 || quad2 || quad3 || quad4) {// this filter removes all events in ANSI space inside any of four quandrangles
//		  mn.hitvec[k]=0;
	          if (quad.hitvec[j]>0 && quad.hitvec[k]>0) {
		    quad.hitvec[k]=0;
		  }
		}
//		if(nsi[j+1][k+1]>-700 && nsi[j+1][k+1]<20000 && !(dubdub||dubsing)) { // 1 = 1n
//		  mn.hitvec[k]=0; 
//		} 
		if(((nsi[j+1][k+1]>nsicut && a0[j+1][k+1]<angmax) || (nsi[j+1][k+1]>0 && a0[j+1][k+1]>angmax)) && !(dubdub||dubsing)) { // 1 = 1n
		  mn.hitvec[k]=0; }
		// check if hit pair has v12 near c and q2 below g_qcut - second hit is likely a gamma dubdub = 0; dubsing = 0;
//	        if ((v[j+1][k+1]>20 && v[j+1][k+1]<40) && nevt.q[k]<g_qcut) leftcurly // check if hit pair has v12 near c and q2 below g_qcut - second hit is likely a gamma
//		  mn.hitvec[k]=0; rightcurly 
//	      }
	      //cout << "done with mn.hitvec" << endl;

	      // this filter applies a traditional causality cut to de-clustered hits
	      if (caus.hitvec[j]>0 && caus.hitvec[k]>0) {
		ccuttest = mn.causCheck(nevt, ccutrad, d[j+1][k+1], v[j+1][k+1]); // 1 if causally related according to traditional causality cut
		if (ccuttest==1) { // check if causally connected
		  caus.hitvec[k]=0; } // update caus vector 
	      }
		//cout << "done with caus.hitvec" << endl;
		  
		if(quad1 || quad2 || quad3 || quad4) { // inside one of the quadrangles
		  quad.hitvec[k]=0; }
		//cout << "done with quad.hitvec" << endl;

	      // this filter applies a d12/a012 space cut to positive NSI events
	      if (pa.hitvec[j]>0 && pa.hitvec[k]>0) {
		  if (d[j+1][k+1]>(0.009*pow(a0[j+1][k+1]-100,2)+40) || dubdub || dubsing) { 
		    pacut = 0; } else { pacut = 1; } // parabola parameterization in d12-a012 space
		  if(((nsi[j+1][k+1]>nsicut && a0[j+1][k+1]<angmax)||(nsi[j+1][k+1]>0 && a0[j+1][k+1]>angmax)) && pacut) { // pacut = 1 (1n)
		    pa.hitvec[k]=0; }
	      }
	    } // for loop for k
	  } // for loop j
	    //cout << "done with causal check loop" << endl;

	  //		    cout << mn.hitvec[0] << mn.hitvec[1] << mn.hitvec[2] << mn.hitvec[3] << mn.hitvec[4] << mn.hitvec[5] << endl;
//		    cout << caus.hitvec[0] << caus.hitvec[1] << caus.hitvec[2] << caus.hitvec[3] << caus.hitvec[4] << caus.hitvec[5] << endl;
//		    cout << "**********" << endl;

// ##########
//
// Assign neutrons their kinematic variables
//
// ##########

	  ncount = 0; // begin counting neutrons from 0
//	  zcount = 0; // 
	  ccount = 0; // 
	  scount = 0; // 
	  qcount = 0;
	  pcount = 0;

	  for (int j=0; j<nevt.mult; j++) { // TODO: hmm, these also look like you could easily throw them into a function
	    // mn-causality cut
	    if (mn.hitvec[j]>0) { // if q is above g_qcut threshold and not causally connected with any earlier scatters
	      mn.clusttype[mn.hitvec[j]]++; // increment clusttype for the type of scatter, i.e. singlet, doublet, etc. 
	      mn.ordertype[mn.ordervec[j]]++; // increment mn.ordertype for the type of z scattering (0=singlet, 1=time reversed scatter, 2=vertical scatter, 3=forward scatter)
	      mn.t[ncount]=nevt.t[j];
	      mn.x[ncount]=nevt.x[j]; 
	      mn.y[ncount]=nevt.y[j]; //assign the neutrons with the appropriate hit values
	      mn.z[ncount]=nevt.z[j]; 
	      mn.q[ncount]=nevt.q[j];
	      ncount++; // add this neutron to the count
	    } 
	    if(caus.hitvec[j]>0){
	      caus.t[ccount]=nevt.t[j];
	      caus.x[ccount]=nevt.x[j];
	      caus.y[ccount]=nevt.y[j];
	      caus.z[ccount]=nevt.z[j];
	      caus.q[ccount]=nevt.q[j];
	      ccount++;
	    }
	    // scattering sites (declustered only (dco), no causality cut)
	    if(dco.hitvec[j]>0){
	      dco.t[scount]=nevt.t[j]; 
	      dco.x[scount]=nevt.x[j]; 
	      dco.y[scount]=nevt.y[j]; 
	      dco.z[scount]=nevt.z[j]; 
	      dco.q[scount]=nevt.q[j]; 
	      scount++;
	    }
	    if(quad.hitvec[j]>0){
	      quad.t[qcount]=nevt.t[j]; 
	      quad.x[qcount]=nevt.x[j]; 
	      quad.y[qcount]=nevt.y[j]; 
	      quad.z[qcount]=nevt.z[j]; 
	      quad.q[qcount]=nevt.q[j]; 
	      qcount++;
	    }
	    if(pa.hitvec[j]>0){
	      pa.t[pcount]=nevt.t[j]; 
	      pa.x[pcount]=nevt.x[j]; 
	      pa.y[pcount]=nevt.y[j]; 
	      pa.z[pcount]=nevt.z[j]; 
	      pa.q[pcount]=nevt.q[j]; 
	      pcount++;
	    }
	  } // j loop
	  //cout << "done with assignment loop" << endl;

// ##########
//
// Compute kinematic scattering variables for first 2 or 3 neutrons
//
// ##########

	  // save nsi12 and a012 computation to tree for easy plotting access
	  mn.nmult = ncount;
	  n1=-1;
	  n2=-1;
	  n3=-1;
	  if (mn.nmult==2) {
	    for (int neut=0; n2<0; neut++) {
	      if (mn.hitvec[neut]>0 && n1<0) {
	        n1=neut;
	      } else if (mn.hitvec[neut]>0 && n2<0) {
		 n2=neut;
	      }
	    }
	    mn.nsi12=nsi[n1+1][n2+1]; 
	    mn.a012=a0[n1+1][n2+1];
	    mn.d12=d[n1+1][n2+1];
	    mn.v12=v[n1+1][n2+1];
	    mn.t12=td[n1+1][n2+1];

	  } else if(mn.nmult>2) {
	    for(int neut=0; n3<0; neut++){
	      if(mn.hitvec[neut]>0 && n1<0){
	 	n1=neut;
	      }else if(mn.hitvec[neut]>0 && n2<0){
	 	n2=neut;
	      }else if(mn.hitvec[neut]>0 && n3<0){
	 	n3=neut;
	      }
	    }
	    mn.nsi12=nsi[n1+1][n2+1]; 
	    mn.nsi13=nsi[n1+1][n3+1]; 
	    mn.nsi23=nsi[n2+1][n3+1]; 
	    mn.a012=a0[n1+1][n2+1];
	    mn.a013=a0[n1+1][n3+1];
	    mn.a023=a0[n2+1][n3+1];
	    mn.d12=d[n1+1][n2+1];
	    mn.d13=d[n1+1][n3+1];
	    mn.d23=d[n2+1][n3+1];
	    mn.v12=v[n1+1][n2+1];
	    mn.v13=v[n1+1][n3+1];
	    mn.v23=v[n2+1][n3+1];
	    mn.t12=td[n1+1][n2+1];
	    mn.t13=td[n1+1][n3+1];
	    mn.t23=td[n2+1][n3+1];
	  }
	  //cout << "done with a012 and nsi12 assignments" << endl;

	  // currently we are only doing the causal quadrangle test on neutrons 1 and 2
	  if(mn.nmult>1) {
	    if(mn.quadrangleAnsiCheck(nevt, nsi[n1+1][n2+1], a0[n1+1][n2+1], m[0]*mn.a012 + b[0], m[1]*mn.nsi12+b[1], m[2]*mn.a012+b[2], m[3]*mn.nsi12+b[3])==1
	      || mn.quadrangleAnsiCheck(nevt, nsi[n1+1][n2+1], a0[n1+1][n2+1], m[4]*mn.a012+b[4], m[5]*mn.nsi12+b[5], m[6]*mn.a012+b[6], m[7]*mn.nsi12+b[7])==1
	      || mn.quadrangleAnsiCheck(nevt, nsi[n1+1][n2+1], a0[n1+1][n2+1], m[8]*mn.a012+b[8], m[9]*mn.nsi12+b[9], m[10]*mn.a012+b[10], m[11]*mn.nsi12+b[11])==1
	      || mn.quadrangleAnsiCheck(nevt, nsi[n1+1][n2+1], a0[n1+1][n2+1], m[12]*mn.a012+b[12], m[13]*mn.nsi12+b[13], m[14]*mn.a012+b[14], m[15]*mn.nsi12+b[15])==1){
	      mn.quad = true; 
	    }
	    if(mn.hitvec[n1]>1 && mn.hitvec[n2]>1){ // also includes dt events
	      mn.dd = true;
	    } else if((mn.hitvec[n1]>1 && mn.hitvec[n2]==1 && nevt.q[n2]>ds_qcut) || (mn.hitvec[n2]>1 && mn.hitvec[n1]==1 && nevt.q[n1]>ds_qcut)){ // preliminary look at old LANL data shows we can drop this q threshold, ~20?
	      mn.ds = true;
	    }
	    if(mn.d12>(0.009*pow(mn.a012-100,2)+40)) {
	      mn.pa = true;
	    }
	    if(mn.causCheck(nevt, ccutrad, mn.d12, mn.v12)==0){
	      mn.cc = true;
	    }
	  }
	  //cout << "done with flags" << endl;
	  // TODO: hmm, could these also maybe become functions?

	  //cout << "caus.nmult: " << caus.nmult << endl;
	  caus.nmult = ccount;
	  if (caus.nmult==2) {
	    n1=-1;
	    n2=-1;
	    for(int neut=0; n2<0; neut++){
	      if(caus.hitvec[neut]>0 && n1<0){
	        n1=neut;
	      }else if(caus.hitvec[neut]>0 && n2<0){
	 	n2=neut;
	      }
	    }
	    caus.nsi12=nsi[n1+1][n2+1]; 
	    caus.a012=a0[n1+1][n2+1];
	    caus.d12=d[n1+1][n2+1];
	    caus.v12=v[n1+1][n2+1];
	    caus.t12=td[n1+1][n2+1];

	  } else if (caus.nmult>2) {
	    n1=-1;
	    n2=-1;
	    n3=-1;
	    for(int neut=0; n3<0; neut++){
	      if(caus.hitvec[neut]>0 && n1<0){
	 	n1=neut;
	      }else if(caus.hitvec[neut]>0 && n2<0){
		n2=neut;
	      }else if(caus.hitvec[neut]>0 && n3<0){
		n3=neut;
	      }
	    }
	    caus.nsi12=nsi[n1+1][n2+1]; 
	    caus.nsi13=nsi[n1+1][n3+1]; 
	    caus.nsi23=nsi[n2+1][n3+1]; 
	    caus.a012=a0[n1+1][n2+1];
	    caus.a013=a0[n1+1][n3+1];
	    caus.a023=a0[n2+1][n3+1];
	    caus.d12=d[n1+1][n2+1];
	    caus.d13=d[n1+1][n3+1];
	    caus.d23=d[n2+1][n3+1];
	    caus.v12=v[n1+1][n2+1];
	    caus.v13=v[n1+1][n3+1];
	    caus.v23=v[n2+1][n3+1];
	    caus.t12=td[n1+1][n2+1];
	    caus.t13=td[n1+1][n3+1];
	    caus.t23=td[n2+1][n3+1];
	  }

	  //cout << "dco.nmult: " << dco.nmult << endl;
  	  dco.nmult = scount;
	  if(dco.nmult==2){
	    n1=-1;
	    n2=-1;
	    for(int neut=0; n2<0; neut++){
	      if(dco.hitvec[neut]>0 && n1<0){
		n1=neut;
	      }else if(dco.hitvec[neut]>0 && n2<0){
		n2=neut;
	      }
	    }
	    dco.nsi12=nsi[n1+1][n2+1]; 
	    dco.a012=a0[n1+1][n2+1];
	    dco.d12=d[n1+1][n2+1];
	    dco.v12=v[n1+1][n2+1];
	    dco.t12=td[n1+1][n2+1];
	  } else if(dco.nmult>2) {
	    n1=-1;
	    n2=-1;
	    n3=-1;
	    for(int neut=0; n3<0; neut++){
	      if(dco.hitvec[neut]>0 && n1<0){
		n1=neut;
	      }else if(dco.hitvec[neut]>0 && n2<0){
		n2=neut;
	      }else if(dco.hitvec[neut]>0 && n3<0){
		n3=neut;
	      }
	    }
	    dco.nsi12=nsi[n1+1][n2+1]; 
	    dco.nsi13=nsi[n1+1][n3+1]; 
	    dco.nsi23=nsi[n2+1][n3+1]; 
	    dco.a023=a0[n2+1][n3+1];
	    dco.d12=d[n1+1][n2+1];
	    dco.d13=d[n1+1][n3+1];
	    dco.d23=d[n2+1][n3+1];
	    dco.v12=v[n1+1][n2+1];
	    dco.v13=v[n1+1][n3+1];
	    dco.v23=v[n2+1][n3+1];
	    dco.t12=td[n1+1][n2+1];
	    dco.t13=td[n1+1][n3+1];
	    dco.t23=td[n2+1][n3+1];
	  }

	  //cout << "quad.nmult: " << quad.nmult << endl;
  	  quad.nmult = qcount;
	  if(quad.nmult==2){
	    n1=-1;
	    n2=-1;
	    for(int neut=0; n2<0; neut++){
	      if(quad.hitvec[neut]>0 && n1<0){
		n1=neut;
	      }else if(quad.hitvec[neut]>0 && n2<0){
		n2=neut;
	      }
	    }
	    quad.nsi12=nsi[n1+1][n2+1]; 
	    quad.a012=a0[n1+1][n2+1];
	    quad.d12=d[n1+1][n2+1];
	    quad.v12=v[n1+1][n2+1];
	    quad.t12=td[n1+1][n2+1];
	  } else if(quad.nmult>2) {
	    n1=-1;
	    n2=-1;
	    n3=-1;
	    for(int neut=0; n3<0; neut++){
	      if(quad.hitvec[neut]>0 && n1<0){
		n1=neut;
	      }else if(quad.hitvec[neut]>0 && n2<0){
		n2=neut;
	      }else if(quad.hitvec[neut]>0 && n3<0){
		n3=neut;
	      }
	    }
	    quad.nsi12=nsi[n1+1][n2+1]; 
	    quad.nsi13=nsi[n1+1][n3+1]; 
	    quad.nsi23=nsi[n2+1][n3+1]; 
	    quad.a012=a0[n1+1][n2+1];
	    quad.a013=a0[n1+1][n3+1];
	    quad.a023=a0[n2+1][n3+1];
	    quad.d12=d[n1+1][n2+1];
	    quad.d13=d[n1+1][n3+1];
	    quad.d23=d[n2+1][n3+1];
	    quad.v12=v[n1+1][n2+1];
	    quad.v13=v[n1+1][n3+1];
	    quad.v23=v[n2+1][n3+1];
	    quad.t12=td[n1+1][n2+1];
	    quad.t13=td[n1+1][n3+1];
	    quad.t23=td[n2+1][n3+1];
	  }

	  //cout << "done with quad assignments" << endl;
 
	  //cout << "pa.nmult: " << pa.nmult << endl;
  	  pa.nmult = pcount;
	  if(pa.nmult==2){
	    n1=-1;
	    n2=-1;
	    for(int neut=0; n2<0; neut++) {
	      if(pa.hitvec[neut]>0 && n1<0) {
		n1=neut;
	      } else if (pa.hitvec[neut]>0 && n2<0) {
		n2=neut;
	      }
	    }
	    pa.nsi12=nsi[n1+1][n2+1]; 
	    pa.a012=a0[n1+1][n2+1];
	    pa.d12=d[n1+1][n2+1];
	    pa.v12=v[n1+1][n2+1];
	    pa.t12=td[n1+1][n2+1];
	  } else if (pa.nmult>2) {
	    n1=-1;
	    n2=-1;
	    n3=-1;
	    for(int neut=0; n3<0; neut++){
	      if(pa.hitvec[neut]>0 && n1<0){
		n1=neut;
	      }else if(pa.hitvec[neut]>0 && n2<0){
		n2=neut;
	      }else if(pa.hitvec[neut]>0 && n3<0){
		n3=neut;
	      }
	    }
	    pa.nsi12=nsi[n1+1][n2+1]; 
	    pa.nsi13=nsi[n1+1][n3+1]; 
	    pa.nsi23=nsi[n2+1][n3+1]; 
	    pa.a012=a0[n1+1][n2+1];
	    pa.a013=a0[n1+1][n3+1];
	    pa.a023=a0[n2+1][n3+1];
	    pa.d12=d[n1+1][n2+1];
	    pa.d13=d[n1+1][n3+1];
	    pa.d23=d[n2+1][n3+1];
	    pa.v12=v[n1+1][n2+1];
	    pa.v13=v[n1+1][n3+1];
	    pa.v23=v[n2+1][n3+1];
	    pa.t12=td[n1+1][n2+1];
	    pa.t13=td[n1+1][n3+1];
	    pa.t23=td[n2+1][n3+1];
	  }
        }
	  //cout << "done with rest of a012 and nsi assingments" << endl;
  
// ##########
//
// Write event data to tree branches
//
// ##########

       	bmn->Fill();
	bcaus->Fill();
	bdco->Fill();
	bquad->Fill();
	bpa->Fill();
       	i++;
	//cout << "done with filling branches" << endl;
  }
  cout << "Finished processing data" << endl;  
  mnt->Write();  // wrote the multi-n root File
  cout << "Wrote root tree" << endl;
  ifile->Close(); // close the input root file
  cout << "Closed input root file" << endl;
  ofile->Close(); // close the output root file
  cout << "Closed output root file" << endl;
  
// ##########
//
// Write settings to configuration file
//
// ##########

  // Open log file (named [outfile.root].log) 
  ofstream log_file;
  string log = ".log";
  string lfile = outfile + log;
  log_file.open(lfile,ios::out);
  if (log_file.is_open()){
    time_t log_time = time(0);
    char* str_log_time = ctime(&log_time);
    log_file<< str_log_time<<endl;
    log_file<< "Data from file: "<<infile<<endl<<endl;
    log_file<< "Datafile type: "<<datatype<< " (1=MoNA experiment, 2=simulation, 3=LANL experiment)"<<endl;
//    log_file<< "Causal test type: "<<causaltest<< " (1=angle-nsi plot, 2=causality cut)"<<endl;
    log_file<< "Using config file: " <<config_file_name[0]<<config_file_name[1]<<endl;
    log_file<< "Config variables:  clustrad: "<<clustrad<<"  seprad: "<<seprad<<"  ccutrad: "<<ccutrad<<"  dtdiff: "<<dtdiff<<"  ebeam: "<<ebeam<<"  (vbeam: "<<mn.vbeam<<")"<<"  g_qcut: "<<g_qcut<<endl;
    log_file<< "Quadrangle 1 x,y coordinates: "<<px[1]<<" "<<py[1]<<" "<<px[2]<<" "<<py[2]<<" "<<px[3]<<" "<<py[3]<<" "<<px[4]<<" "<<py[4]<<endl;
    log_file<< "Quadrangle 2 x,y coordinates: "<<px[5]<<" "<<py[5]<<" "<<px[6]<<" "<<py[6]<<" "<<px[7]<<" "<<py[7]<<" "<<px[8]<<" "<<py[8]<<endl;
    log_file<< "Quadrangle 3 x,y coordinates: "<<px[9]<<" "<<py[9]<<" "<<px[10]<<" "<<py[10]<<" "<<px[11]<<" "<<py[11]<<" "<<px[12]<<" "<<py[12]<<endl;
    log_file<< "Quadrangle 4 x,y coordinates: "<<px[13]<<" "<<py[13]<<" "<<px[14]<<" "<<py[14]<<" "<<px[15]<<" "<<py[15]<<" "<<px[16]<<" "<<py[16]<<endl;
    log_file<<"Total entries: "<<nentries<<endl;
    cout<< "Metadata stored in log file "<<lfile<<endl;
  } else {
    cout<< "Failed to open log file"<<endl;
  }
  log_file.close();

  return 0;
}

//##########
//
// mnProc class methods
//
//##########

Int_t mnProc::quadrangleAnsiCheck(MNEVT nevt, double nsivalue, double a012value, double nsimin, double amax, double nsimax, double amin) 
{
	if(nsivalue>nsimin && a012value<amax && nsivalue<nsimax && a012value>amin){ 
	  return 1;
	} else {return 0;}
}

// optimized for the RIKEN arrays, some elements like the wall gap were difficult/impossible to reproduce for MoNA setup
// it's fair to look at this method to contrast with, say, our declustering, but it's not fair to compare our imitation of their code with our experiments and show statistics
Int_t mnProc::rikenCheck(MNEVT nevt, int j, int k, float ddiff, float tdiff, float betadiff, float qdiff, float v01){
	// Recoil Proton Removal:
	// for same wall events, not exactly sure how to query that quite yet but the time and distance should be plenty large enough on different walls anyway
	if(sqrt(0.5*pow(d[j+1][k+1]/ddiff, 2)+0.5*pow(td[j+1][k+1]/tdiff,2))<1){
	//if(sqrt(0.5*pow(p[j+1][k+1]/pdiff, 2) + 0.5*pow(td[j+1][k+1]/tdiff,2)) < 1 ){ // limits hypersphere up to path length pdiff or time difference tdiff
		return 2;
	}
	// for separate wall events, we don't have their charged particle detectors
	
	// Gamma Crosstalk:
	// square cut out of 1/beta12 vs. q2 graph- use betadiff as the distance between 1 and the edge of the square 
	//if((29.9792458/v[j+1][k+1] < (1+betadiff)) && (29.9792458/v[j+1][k+1] > (1-betadiff))){
	if((29.9792458/v[j+1][k+1] < (1+betadiff)) && (29.9792458/v[j+1][k+1] > (1-betadiff)) && (nevt.q[k]<qdiff)){
		return 1;
	}

	// Neutron Crosstalk:
	// beta01/beta12 < 1 (with sign: backscatters have negative velocity, which isn't computed by our path length algorithm, so z of n2 > z of n1
	if(v01/ v[j+1][k+1]>1){
//	if(v01/ v[j+1][k+1]>1 && nevt.z[k]>nevt.z[j]){
		return 1;
	} //else{return 0;}
	return 0;
}
Int_t mnProc::causCheck(MNEVT nevt, float ccutrad, float ddiff, float vdiff) // performs traditional causality cut 
{
        if (ddiff<ccutrad || vdiff<vbeam) {
          return 1; // event falls in "causal zone" 
	} else {return 0;} // non-causally related events
}
Int_t mnProc::qRescueCheck(MNEVT nevt, int j, int k, float qtithe) // tests if both hits are above a threshold q, didn't see a very conclusive value
{
	if(q[j]>qtithe && q[k]>qtithe){
	  return 0; // deposited too much energy to assume this is still 1n scattering
	} else {return 1;} // causally related events
}
Int_t mnProc::ptCheck(MNEVT nevt, int j, int k, float d_slope, float d_intercept, float t_slope, float t_intercept, float t_width_slope, float t_width_intercept ) {
	double dz = abs(nevt.z[k]-nevt.z[j]); // distance in z (bar combination)
	// P_max(dz) = m*dz + b; t(dz)=m*dz + b +/- t_width; t_width(dz) = m*dz + b
	if(d[j+1][k+1] < d_slope*dz + d_intercept && td[j+1][k+1] < t_slope*dz + t_intercept + t_width_slope*dz + t_width_intercept 
			&& td[j+1][k+1] > t_slope*dz + t_intercept - t_width_slope*dz - t_width_intercept){
		return 1;
	} else {
		return 0;
	}
}
Int_t mnProc::mnReset() //zero out all mn variables
{
        for (int j=0; j<16; j++) {
	  t[j]=0; x[j]=0; y[j]=0; z[j]=0; q[j]=0; hitvec[j]=-1; clusttype[j]=0; ordervec[j]=-1; //hitvec[j]=0;
	  for (int k=0; k<16; k++) {
	    d[j][k]=-1;
            td[j][k]=-1;
	    v[j][k]=-1;
	    a0[j][k]=-1;
	    nsi[j][k]=-1;
	  }
	  if(j<4){ordertype[j]=0;}
	}
	d12=0; d13=0; d23=0;
	v12=0; v13=0; v23=0;
	t12=0; t13=0; t23=0;
	nmult=0; multn1=0; multn2=0; multn3=0; a012=-5; nsi12=-5; quad=false; dd=false; ds=false; pt=false; pa=false; cc=false;
	return 1;
}
